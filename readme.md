## How to run

1. `git clone https://Yiin@bitbucket.org/Yiin/seohelis-task2.git`
1. `npm install`
1. `gulp`
1. `composer install`
1. `php artisan key:generate`
1. Configure .env (database entries)
1. `php artisan migrate`
1. `php artisan user:create <email>`
1. `php artisan serve`
1. Go to site, login with created account, add rss source urls
1. `php artisan feed:update`