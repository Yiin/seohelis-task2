/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('data-table', require('./components/DataTable.vue'));
Vue.component('row', require('./components/Row.vue'));
Vue.component('delete-button', require('./components/DeleteButton.vue'));
Vue.component('feed', require('./components/Feed.vue'));

Vue.component('modal', {
    template: '#modal-template'
});

const app = new Vue({
    el: '#app'
});