@extends ('layouts.app')

@section ('content')

    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                Create new category
            </div>

            <div class="panel-body">
                <form action="{{ route('category.store') }}"
                      method="POST">
                    {!! csrf_field() !!}

                    @if(isset($category))
                        <input type="hidden" name="category_id" value="{{ $category->id }}">
                    @endif

                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input type="text" name="name" class="form-control"
                               value="{{ isset($category) ? $category->name : old('name') }}">
                    </div>
                    <div class="form-group">
                        <label>Feeds:</label>
                        <select multiple name="sources[]" class="form-control">
                            @foreach($sources as $source)
                                <option value="{{ $source->id }}" @if(isset($category)) {{ $category->sources->where('id', $source->id)->count() ? 'selected' : '' }} @endif>
                                    {{ $source->title }} - {{ $source->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit"
                            class="btn btn-primary">{{ isset($category) ? 'Update category' : 'Create category' }}</button>
                </form>
            </div>
        </div>
    </div>

@endsection