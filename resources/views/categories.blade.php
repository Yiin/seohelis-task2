@extends ('layouts.app')

@section ('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Categories
                        <a href="{{ route('category.create') }}" class="btn btn-xs btn-primary"
                           style="margin-left: 10px">
                            add new
                        </a>
                    </div>

                    <div class="panel-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th width="150px"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($categories as $category)
                                <tr>
                                    <td>{{ $category->name }}</td>
                                    <td>
                                        <a class="btn btn-sm btn-default"
                                           href="{{ route('category.edit', ['id' => $category->id]) }}">
                                            Edit
                                        </a>
                                        <delete-button url="{{ route('category.destroy', ['id' => $category->id]) }}"
                                                       classes="btn btn-sm btn-danger">
                                            Delete
                                        </delete-button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection