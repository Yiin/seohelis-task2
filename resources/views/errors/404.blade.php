<!DOCTYPE html>
<html>
<head>
    <title>404 Not Found.</title>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600&subset=latin-ext" rel="stylesheet">

    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            color: #B0BEC5;
            display: table;
            font-weight: 100;
            font-family: 'Open Sans', sans-serif;
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 72px;
            margin-bottom: 40px;
        }

        a {
            font-size: 24px;
            text-decoration: none;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="content">
        <div class="title">404 Not Found.</div>
        <a href="/">Grįžti į pradžią.</a>
    </div>
</div>
</body>
</html>
