@extends ('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-default">
            <div class="panel-body">
                <feed json="{{ $categories->toJson() }}"></feed>
            </div>
        </div>
    </div>

@endsection