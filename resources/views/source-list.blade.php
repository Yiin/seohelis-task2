@extends ('layouts.app')

@section ('content')

    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">Feed list</div>

            <div class="panel-body">
                <data-table
                        name="source"
                        items="{{ $sources->toJson() }}"
                        columns='[
                                    { "title": "Title", "prop": "title" },
                                    { "title": "Name", "prop": "name" },
                                    { "title": "URL", "prop": "url", "isLink": true },
                                    { "title": "RSS URL", "prop": "rss_url", "isLink": true }
                                ]'
                ></data-table>
            </div>
        </div>
    </div>

@endsection