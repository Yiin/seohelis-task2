<?php

namespace App\Console\Commands;

use App\Services\AdminService;
use App\User;
use Illuminate\Console\Command;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $adminService;

    /**
     * Create a new command instance.
     * @param AdminService $adminService
     */
    public function __construct(AdminService $adminService)
    {
        parent::__construct();

        $this->adminService = $adminService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $password = $this->adminService->createNewAdminUser($this->argument('email'));

        echo "Generated password: {$password}" . PHP_EOL;
    }
}
