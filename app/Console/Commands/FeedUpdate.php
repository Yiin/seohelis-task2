<?php

namespace App\Console\Commands;

use App\Feed;
use App\Services\RssFeedService;
use App\Source;
use Carbon\Carbon;
use DateTime;
use Illuminate\Console\Command;

/**
 * Class FeedUpdate
 * @package App\Console\Commands
 */
class FeedUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update feed';

    /**
     * @var RssFeedService
     */
    protected $rssFeedService;

    /**
     * Create a new command instance.
     * @param RssFeedService $rssFeedService
     */
    public function __construct(RssFeedService $rssFeedService)
    {
        parent::__construct();

        $this->rssFeedService = $rssFeedService;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $log = $this->rssFeedService->update();

        foreach ($log['info'] as $info) {
            echo "Fetched {$info['fetched_items']} items from RSS source {$info['source']}." . PHP_EOL;
        }
        $amountOfSources = count($log['info']);

        echo "Total added items from {$amountOfSources} source(s): {$log['total']}" . PHP_EOL;
        echo "Found duplicates: {$log['duplicates']}" . PHP_EOL;
    }
}
