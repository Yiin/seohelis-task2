<?php

namespace App\Http\Controllers;

use App\Source;
use Illuminate\Http\Request;

/**
 * Class SourceController
 * @package App\Http\Controllers
 */
class SourceController extends Controller
{
    /**
     * @return mixed
     */
    public function index()
    {
        $sources = Source::all();

        return view('source-list')->with(compact('sources'));
    }

    /**
     * @param Request $request
     * @return \App\Source
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'name' => 'required',
            'url' => 'required',
            'rss_url' => 'required'
        ]);

        $source = Source::create($request->only(['title', 'name', 'url', 'rss_url']));

        return $source;
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'name' => 'required',
            'url' => 'required',
            'rss_url' => 'required'
        ]);

        $source = Source::find($id);

        if (!$source) {
            return response("Invalid ID", 400);
        }

        $source->update($request->only(['title', 'name', 'url', 'rss_url']));

        return $source;
    }

    /**
     * @param $id
     * @return string
     */
    public function destroy($id)
    {
        Source::destroy($id);

        return 'OK';
    }
}
