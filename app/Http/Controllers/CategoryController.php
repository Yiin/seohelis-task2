<?php

namespace App\Http\Controllers;

use App\Category;
use App\Source;
use Illuminate\Http\Request;

/**
 * Class CategoryController
 * @package App\Http\Controllers
 */
class CategoryController extends Controller
{
    /**
     * @return mixed
     */
    protected function index()
    {
        $categories = Category::all();

        return view('categories')->with(compact('categories'));
    }

    /**
     * @param $id
     * @return mixed
     */
    protected function view($id)
    {
        return Category::find($id);
    }

    /**
     * @return mixed
     */
    protected function create()
    {
        $sources = Source::all();

        return view('category.create-or-update')->with(compact('sources'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'category_id' => 'exists:categories,id'
        ]);

        if ($request->has('category_id')) {
            return $this->update($request, $request->get('category_id'));
        }

        $category = Category::create($request->only([
            'name'
        ]));

        $category->sources()->sync($request->get('sources', []));

        return redirect()->route('category.index');
    }

    /**
     * @param $id
     * @return mixed
     */
    protected function edit($id)
    {
        $category = Category::with('sources')->find($id);

        if ($category) {
            $sources = Source::all();

            return view('category.create-or-update')->with(compact('category', 'sources'));
        }
        return redirect()->route('category.index');
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $category = Category::find($id);

        if ($category) {
            $category->update($request->only([
                'name'
            ]));
            $category->sources()->sync($request->get('sources', []));
        }

        return redirect()->route('category.index');
    }

    /**
     * @param $id
     * @return int
     */
    protected function destroy($id)
    {
        return Category::destroy($id);
    }
}
