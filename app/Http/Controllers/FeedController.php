<?php

namespace App\Http\Controllers;

use App\Category;
use App\Feed;
use Illuminate\Http\Request;

/**
 * Class FeedController
 * @package App\Http\Controllers
 */
class FeedController extends Controller
{
    /**
     * @param null $category_id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Database\Eloquent\Collection|\Symfony\Component\HttpFoundation\Response|static[]
     */
    protected function feed($category_id = null)
    {
        if ($category_id) {
            $category = Category::with('sources')->find($category_id);

            if (!$category) {
                return response(400);
            }

            $query = Feed::whereIn('source_id', $category->sources->pluck('id'));
        } else {
            $query = Feed::query();
        }

        return $query->with('source')->orderBy('published_at', 'desc')->limit(30)->get();
    }
}
