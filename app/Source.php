<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Source
 * @package App
 */
class Source extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['title', 'name', 'url', 'rss_url'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function feeds()
    {
        return $this->hasMany(Feed::class);
    }
}
