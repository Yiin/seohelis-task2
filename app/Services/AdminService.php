<?php

namespace App\Services;

use App\User;

class AdminService
{
    public function createNewAdminUser($email)
    {
        $user = User::firstOrNew([
            'name' => 'admin'
        ]);

        if (!$user->exists) {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                throw new \Exception('Please enter valid email address.');
            }

            $generatedPassword = str_random();

            $user->fill([
                'email' => $email,
                'password' => bcrypt($generatedPassword)
            ]);
            $user->save();

            return $generatedPassword;
        } else {
            throw new \Exception('User already exists.');
        }
    }

}