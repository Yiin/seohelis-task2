<?php

namespace App\Services;

use App\Feed;
use App\Source;
use Carbon\Carbon;

class RssFeedService
{
    public function update()
    {
        $log = [
            'duplicates' => 0,
            'total' => 0,
            'info' => []
        ];

        foreach ($this->sources() as $source) {
            $this->loadRssFeed($source, $log);
        }

        return $log;
    }

    public function sources()
    {
        return Source::with('feeds')->get();
    }

    public function loadRssFeed($source, &$log)
    {
        // pakraunam xml iš šaltinio
        $xml_string = self::get_xml_from_url(trim($source->rss_url));

        // xml to object
        $xml = simplexml_load_string($xml_string);

        // jeigu kas nors nepavyko skipinam
        if (!$xml) {
            return;
        }

        // einam per kiekvieną rss feed itemą
        foreach ($xml->channel->item as $item) {
            // nesaugom straipsnių su tuo pačiu pavadinimu iš to paties source
            if ($source->feeds->where('title', $item->title)->count()) {
                $log['duplicates']++;
                continue;
            }
            // įrašom naują straipsnį į db
            Feed::create([
                'title' => $item->title,
                'description' => $item->description,
                'link' => $item->link,
                'published_at' => Carbon::createFromFormat("D, d M Y H:i:s T", $item->pubDate),
                'source_id' => $source->id
            ]);
            $log['total']++;
        }

        $log['info'][] = [
            'source' => "{$source->title} - {$source->name}",
            'fetched_items' => count($xml->channel->item)
        ];
    }

    /**
     * @param $url
     * @return mixed
     */
    private static function get_xml_from_url($url)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT,
            'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

        $xmlstr = curl_exec($ch);
        curl_close($ch);

        return $xmlstr;
    }
}