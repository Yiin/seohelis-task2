<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Feed
 * @package App
 */
class Feed extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['title', 'description', 'link', 'source_id', 'published_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function source()
    {
        return $this->belongsTo(Source::class);
    }
}
