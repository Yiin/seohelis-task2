<?php

namespace App\Providers;

use App\Services\AdminService;
use App\Services\RssFeedService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(RssFeedService::class);
        $this->app->singleton(AdminService::class);
    }
}
