<?php

use App\Feed;
use App\Services\RssFeedService;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

/**
 * Class ExampleTest
 */
class FeedUpdateTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @var RssFeedService
     */
    protected $rssFeedService;

    /**
     * ExampleTest constructor.
     * @param null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->rssFeedService = resolve('App\Services\RssFeedService');
    }

    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testFeedUpdate()
    {
        $rss_url = 'http://www.feedforall.com/sample.xml';

        $this->assertEquals(0, $this->rssFeedService->sources()->count());

        \App\Source::create([
            'title' => 'Test',
            'name' => 'test',
            'url' => 'http://www.example.com',
            'rss_url' => $rss_url,
        ]);

        $this->assertEquals(1, $this->rssFeedService->sources()->count());

        $this->assertEquals(0, Feed::count());

        $ret = $this->rssFeedService->update();

        $this->assertEquals($ret['total'], Feed::count());
    }
}
