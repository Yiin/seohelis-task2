<?php

use App\Services\AdminService;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

/**
 * Class ExampleTest
 */
class CreateUserTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @var AdminService
     */
    protected $adminService;

    /**
     * ExampleTest constructor.
     * @param null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->adminService = resolve('App\Services\AdminService');
    }

    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testUserCreation()
    {
        $email = 'sally@example.com';

        $this->adminService->createNewAdminUser($email);

        $this->seeInDatabase('users', [
            'email' => $email
        ]);

        // TODO: sukurti normalias exception klases
        $this->expectException('\Exception');
        $this->adminService->createNewAdminUser($email);
    }
}
